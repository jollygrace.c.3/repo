const Course = require('../models/course')


// Activity 1.
//lets create a function that will allow us to add a new course inside the database. 
module.exports.insert = (params) => {
  let course = new Course({
  	name: params.name,
  	description: params.description,
  	price: params.price,
    isActive: params.isActive,
    createdOn: params.createdOn
  }) 
  return course.save().then((course, err) => {
     return (err) ? false : true 
  })
}


//Activity 2.
//we are going to create a new function that will display all of the courses that has an active status of "true". 
module.exports.getAll = () => {
    return Course.find({ isActive: true }).then(courses => courses)
}

//Activity 3.
//we are going to create anothe function that will display a single course. 
module.exports.showOne = (courseId) => {
    return Course.findById(courseId).then({}, (err, result)=>{
    if(err){
      console.log(err);
      return false;
      }
})
}

//Activty 4
//We'll update a course here
module.exports.update = (courseId, updateCourse) => { 
  return Course.findById(courseId).then({}, (err, result)=>{
    if(err){
      console.log(err);
      return false;
      }
  result.name = updateCourse.name;
  result.description = updateCourse.description;
  result.price = updateCourse.price;

  return result.save().then((updatedCourse, err) => {
    if(err){
      console.log(err);
      return false;
      }else{
        return updatedCourse
      }
  })        
})

}
